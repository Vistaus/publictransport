#!/bin/bash

cd ../..
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y # how to use rust sdk extension instead?
source $HOME/.cargo/env
meson builddir && cd builddir
meson dist