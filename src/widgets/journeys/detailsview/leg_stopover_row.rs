use gettextrs::gettext;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::StopOver;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/journeys_leg_stopover_row.ui")]
    pub struct StopoverRow {
        #[template_child]
        pub planned_departure: TemplateChild<gtk::Label>,
        #[template_child]
        pub departure_delay: TemplateChild<gtk::Label>,
        #[template_child]
        pub planned_arrival: TemplateChild<gtk::Label>,
        #[template_child]
        pub arrival_delay: TemplateChild<gtk::Label>,
        #[template_child]
        pub stop: TemplateChild<gtk::Label>,
        #[template_child]
        pub stop_track: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for StopoverRow {
        const NAME: &'static str = "JourneyStopoverRow";
        type Type = super::StopoverRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for StopoverRow {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
        }
    }
    impl WidgetImpl for StopoverRow {}
    impl ListBoxRowImpl for StopoverRow {}
}

glib::wrapper! {
    pub struct StopoverRow(ObjectSubclass<imp::StopoverRow>)
        @extends gtk::Widget, gtk::ListBoxRow,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Actionable;
}

impl StopoverRow {
    pub fn new(stopover: &StopOver) -> Self {
        let stopover_row: StopoverRow =
            glib::Object::new(&[]).expect("Failed to create StopoverRow");

        stopover_row.fill(stopover);
        stopover_row
    }

    pub fn fill(&self, stopover: &StopOver) -> &Self {
        let dep_planned = &stopover.plannedDeparture.unwrap(); // TODO accept of there is no departure planned
        let arr_planned = &stopover.plannedArrival.unwrap();  // TODO accept if there is no arrival planned

        // TODO create help function (can be used by journeyRow as well)
        let str_dep_delay = match &stopover.departure {
            Some(d) => {
                let mut str_dep_delay = String::from("+");
                str_dep_delay.push_str(
                    &d.signed_duration_since(*dep_planned)
                        .num_minutes()
                        .to_string(),
                );
                str_dep_delay
            }
            None => String::from(""),
        };
        let str_arr_delay = match &stopover.arrival {
            Some(d) => {
                let mut str_arr_delay = String::from("+");
                str_arr_delay.push_str(
                    &d.signed_duration_since(*arr_planned)
                        .num_minutes()
                        .to_string(),
                );
                str_arr_delay
            }
            None => String::from(""),
        };

        // TODO create help function (can be used by journeyRow as well)
        let stop_track = match &stopover.departurePlatform {
            Some(d) => {
                let mut str = gettext("Pl. ").to_string();
                str.push_str(&d);
                str
            }
            None => "".to_string(),
        };

        let imp = self.imp();

        imp.planned_departure
            .get()
            .set_markup(&dep_planned.format("%H:%M").to_string());
        imp.departure_delay.get().set_markup(&str_dep_delay);
        imp.planned_arrival
            .get()
            .set_markup(&arr_planned.format("%H:%M").to_string());
        imp.arrival_delay.get().set_markup(&str_arr_delay);
        imp.stop_track.get().set_markup(&stop_track);
        imp.stop.get().set_markup(&stopover.stop.name.clone());

        self
    }
}
