use crate::models::client::HafasClient;
use chrono::prelude::*;
use glib::clone;
use glib::value::Value;
use glib::ParamSpecObject;
use glib::{ParamFlags, ParamSpec};
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::TimeType;
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/datetime_view.ui")]
    pub struct DateTimeView {
        #[template_child]
        pub time_hh: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub time_mm: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub calendar: TemplateChild<gtk::Calendar>,
        #[template_child]
        pub check_departure: TemplateChild<gtk::CheckButton>,
        #[template_child]
        pub check_arrival: TemplateChild<gtk::CheckButton>,
        #[template_child]
        pub ok_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub now_button: TemplateChild<gtk::Button>,
        pub client: RefCell<HafasClient>, // only store datetime?
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DateTimeView {
        const NAME: &'static str = "DateTimeView";
        type Type = super::DateTimeView;
        type ParentType = gtk::Box; // SearchEntry is not subclassable, so we put it in a box

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for DateTimeView {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    // Name
                    "client",
                    // Nickname
                    "client",
                    // Short description
                    "client",
                    // Default value
                    HafasClient::static_type(),
                    // The property can be read and written to
                    ParamFlags::READWRITE,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "client" => {
                    let client = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.client.replace(client);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "client" => self.client.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.setup_datetime_view();

            obj.connect_notify_local(Some("client"), move |datetime_view, _| {
                datetime_view.setup_datetime_view();
            });

            self.ok_button.connect_clicked(clone!(
                @strong obj => move |_| {
                let year = obj.imp().calendar.year() as i32;
                let month = obj.imp().calendar.month() as u32;
                let day = obj.imp().calendar.day() as u32;
                let dt = Local.ymd(year, month+1, day)
                    .and_hms(obj.imp().time_hh.value_as_int() as u32,
                    obj.imp().time_mm.value_as_int() as u32, 0);

                let hafas_client = obj.property::<HafasClient>("client");
                let client = hafas_client.get();

                *client.journeys_config.datetime.lock().unwrap() = Some(dt);
                if obj.imp().check_departure.is_active() == true {
                    client.set_datetime_type(TimeType::Departure);
                } else {
                    client.set_datetime_type(TimeType::Arrival);
                }
                let hafas_client_new = HafasClient::new(&client);
                obj.set_property("client", hafas_client_new);

                let _ = obj.activate_action("win.show-main-leaflet", None);
            }));

            self.now_button.connect_clicked(clone!(
                @strong obj => move |_| {
                let hafas_client = obj.property::<HafasClient>("client");
                let client = hafas_client.get();

                *client.journeys_config.datetime.lock().unwrap() = None;
                if obj.imp().check_departure.is_active() == true {
                    client.set_datetime_type(TimeType::Departure);
                } else {
                    client.set_datetime_type(TimeType::Arrival);
                }

                let hafas_client_new = HafasClient::new(&client);
                obj.set_property("client", hafas_client_new);

                let _ = obj.activate_action("win.show-main-leaflet", None);
            }));
        }
    }
    impl WidgetImpl for DateTimeView {}
    impl BoxImpl for DateTimeView {}
}

glib::wrapper! {
    pub struct DateTimeView(ObjectSubclass<imp::DateTimeView>)
        @extends gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Editable;
}

impl DateTimeView {
    pub fn new() -> Self {
        let date_time_view: DateTimeView =
            glib::Object::new(&[]).expect("Failed to create DateTimeView");

        date_time_view.setup_datetime_view();

        date_time_view
    }

    pub fn setup_datetime_view(&self) {
        let hafas_client = self.property::<HafasClient>("client");
        let datetime_option = hafas_client
            .get()
            .journeys_config
            .datetime
            .lock()
            .unwrap()
            .clone();
        let datetime_type = hafas_client.get().get_datetime_type().clone();

        let datetime = match datetime_option {
            Some(dt) => dt,
            None => Local::now(),
        };

        if std::mem::discriminant(&datetime_type) == std::mem::discriminant(&TimeType::Arrival) {
            self.imp().check_departure.set_active(false);
        } else {
            self.imp().check_departure.set_active(true);
        }

        self.imp().calendar.set_property(
            "year",
            datetime.format("%Y").to_string().parse::<i32>().unwrap(),
        );
        self.imp().calendar.set_property(
            "month",
            datetime.format("%m").to_string().parse::<i32>().unwrap() - 1,
        );
        self.imp().calendar.set_property(
            "day",
            datetime.format("%d").to_string().parse::<i32>().unwrap(),
        );
        self.imp()
            .time_hh
            .set_value(datetime.format("%H").to_string().parse::<f64>().unwrap());
        self.imp()
            .time_mm
            .set_value(datetime.format("%M").to_string().parse::<f64>().unwrap());
    }
}

impl std::default::Default for DateTimeView {
    fn default() -> Self {
        glib::Object::new(&[]).expect("Failed to create DateTimeView")
    }
}
