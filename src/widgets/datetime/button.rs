use crate::models::client::HafasClient;
use gettextrs::gettext;
use glib::{Object, Value};
use glib::{ParamFlags, ParamSpec, ParamSpecObject};
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct DatetimeButton {
        pub client: RefCell<HafasClient>,
    }

    // The central trait for subclassing a GObject
    #[glib::object_subclass]
    impl ObjectSubclass for DatetimeButton {
        const NAME: &'static str = "DatetimeButton";
        type Type = super::DatetimeButton;
        type ParentType = gtk::Button;
    }

    // Trait shared by all GObjects
    impl ObjectImpl for DatetimeButton {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::new(
                    // Name
                    "client",
                    // Nickname
                    "client",
                    // Short description
                    "client",
                    // Default value
                    HafasClient::static_type(),
                    // The property can be read and written to
                    ParamFlags::READWRITE,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "client" => {
                    let client = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.client.replace(client);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "client" => self.client.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.connect_notify_local(Some("client"), move |datetime_button, _| {
                let hafas_client = datetime_button.property::<HafasClient>("client");
                let datetime = hafas_client.get().journeys_config.datetime;
                match datetime.lock().unwrap().clone() {
                    Some(dt) => {
                        datetime_button.set_label(&dt.format("%d.%m.%Y, %H:%M").to_string())
                    }
                    None => datetime_button.set_label(&gettext("Now".to_string())),
                };
            });
        }
    }

    // Trait shared by all widgets
    impl WidgetImpl for DatetimeButton {}

    // Trait shared by all buttons
    impl ButtonImpl for DatetimeButton {
        fn clicked(&self, button: &Self::Type) {
            // self.number.set(self.number.get() + 1);
            // button.set_label(&self.number.get().to_string())

            let _ =
                button.activate_action("win.show-leaflet-page", Some(&"date_time".to_variant()));
        }
    }
}

glib::wrapper! {
    pub struct DatetimeButton(ObjectSubclass<imp::DatetimeButton>)
    @extends gtk::Button, gtk::Widget,
    @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl DatetimeButton {
    pub fn new() -> Self {
        Object::new(&[]).expect("Failed to create `DatetimeButton`.")
    }

    pub fn with_label(label: &str) -> Self {
        Object::new(&[("label", &label)]).expect("Failed to create `DatetimeButton`.")
    }
}

impl std::default::Default for DatetimeButton {
    fn default() -> Self {
        glib::Object::new(&[]).expect("Failed to create DatetimeButton")
    }
}
