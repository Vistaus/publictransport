use gtk::subclass::prelude::*;
use hafas_client::Leg;
use std::cell::RefCell;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct HafasLeg {
        pub leg: RefCell<Option<Leg>>,
    }

    impl std::default::Default for HafasLeg {
        fn default() -> Self {
            HafasLeg {
                leg: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for HafasLeg {
        const NAME: &'static str = "HafasLeg";
        type Type = super::HafasLeg;

        fn new() -> Self {
            Self {
                leg: RefCell::new(None),
            }
        }
    }

    impl ObjectImpl for HafasLeg {}
}

glib::wrapper! {
    pub struct HafasLeg(ObjectSubclass<imp::HafasLeg>);
}

impl HafasLeg {
    pub fn new(leg: Leg) -> Self {
        let hafas_leg: HafasLeg = glib::Object::new(&[]).expect("Failed to create HafasLeg");
        *hafas_leg.imp().leg.borrow_mut() = Some(leg);
        hafas_leg
    }

    pub fn get(&self) -> Option<Leg> {
        let leg_ref = self.imp().leg.borrow();
        leg_ref.clone()
    }
}

impl std::default::Default for HafasLeg {
    fn default() -> Self {
        glib::Object::new(&[]).expect("Failed to create HafasLeg")
    }
}
