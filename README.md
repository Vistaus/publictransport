# Public Transport

<a href="https://flathub.org/apps/details/com.gitlab.maevemi.publictransport">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

Bring Real time information for public transportation (currently in Germany)
to Linux phones/the Linux desktop (the basic features of the likes of `DB Navigator` or `Transportr`).
Also learn some Rust in the process.

Pull requests welcome. If you plan to make bigger changes please open an issue first to discuss the matter.

<a id="screenshots"></a>

# Screenshots


<img src="./data/screenshots/publictransport_screenshot_1.png" alt="main_view" width="300"/>
<img src="./data/screenshots/publictransport_screenshot_2.png" alt="details_view" width="300"/>

<a id="installation_usage"></a>

# Installation/Usage


<a id="installation_flatpack"></a>

## Using flatpack

Make sure you have flatpak installed

    # clone repo or retrieve source files otherwise
    flatpak-builder --repo=<myrepo> <buildfolder> com.gitlab.maevemi.publictransport.json
    flatpak build-bundle <myrepo> <output_file>.flatpak com.gitlab.maevemi.publictransport
    flatpak install <output_file>.flatpak
  

<a id="installation_meson"></a>

## Build/install yourself using Meson/Ninja

    # clone repo or retrieve source files otherwise
    cd publictransport; # enter project folder
    meson . build
    ninja -C build
    sudo ninja -C build install

<a id="tech_stack"></a>

# Tech Stack

Public Transport heavily depends on `GTK`, `libadwaita` and their Rust bindings `gtk-rs` for the GUI and the crate `hafas-client-rs` which gets the
information directly from the hafas endpoint

-   **GTK:** <https://gtk-rs.org/>
-   **libadwaita** <https://gnome.pages.gitlab.gnome.org/libadwaita/doc/>
-   **gtk-rs** <https://gtk-rs.org/>
-   **hafas-client-rs** <https://gitlab.com/maevemi/hafas-client-rs>


<a id="development"></a>

# Development

For development purposes, we do not want to install the full application on every recompile.


<a id="development_flatpak"></a>

## Flatpak / Gnome-builder

Make sure to have Flatpak installed.
Then start Gnome Builder, clone this respository, let Gnome Builder download the 
necessary sdks and click the play button (gnome builder will compile and start 
public Transport).

cross compile from x86 to arm (e. g. for the Pinephone):

    # clone repo or retrieve source files otherwise
    flatpak-builder --arch=aarch64 --repo=<myrepo> <buildfolder> com.gitlab.maevemi.publictransport.json
    flatpak build-bundle --arch=aarch64 <myrepo> <output_file>.flatpak com.gitlab.maevemi.publictransport
    
Then transfer the  '<output_file>.flatpak' file to the phone (e. g. using scp) and install it with flatpak install <output_file>.flatpak
More info about cross-compling with flatpak: https://developer.puri.sm/Librem5/Apps/Packaging_Apps/Building_Flatpaks/Cross-Building.html

<a id="development_meson"></a>

## Using only Meson/Ninja

This is just a very temporary workaround until we figure out how to do this
canonically. We have to workaround the problem that a binary simply compiled
with `meson . build` and `ninja -C build` expects to be followed by `sudo ninja
-C build install` and subsequently required files to be found under `/usr/local`
or similar. But we do not want to install the application on every development
iteration. While another aproach would be to assign some build folder in our
project folder as the installation location and call `DESTDIR=/path/to/staging
meson install -C build` (and therefore at least do not need `sudo` anymore) we
first try to skip the installation proces.

    cd publictransport/ # enter project folder
    # replace @pkgdatadir@ with build/src in config.rs.in
    sed -i 's,@pkgdatadir@;,"'"$(pwd)"'/build/src";//@pkgdatadir@;,' src/config.rs.in
    # build with meson / ninja
    meson . build
    ninja -C build

After that we can iteratively compile & run our program using

    ninja -C build && ./build/target/debug/publictransport


<a id="other_resources"></a>

# Other Resources

-   Building with Meson/Ninja
    -   **Meson Tutorial:** <https://mesonbuild.com/Tutorial.html>
    -   **Building for Purism:** <https://developer.puri.sm/Librem5/Apps/Tutorials/App_Resources/Building_the_App.html>
-   GTK Resources
    -   **Rust GTK Examples:** <https://github.com/gtk-rs/gtk-rs/tree/master/examples/src/bin>
    -   **Rust GTK Docs:** <https://gtk-rs.org/docs/gtk/index.html>
